# CBC Holodeck Web Version
## Project done by OCAD U VALab

### Overview

The CBC Holodeck web version is the original prototype for the CBC Newsroom Holodeck which was built after this concept/prototype was completed and the concept solidified. This web based application is a visual keyword search program which uses video as its medium. The application allows users to select a one video from a grid of videos displayed on a touch screen, all of which are playing the videos and looping indefinitly. Once a video is selected, the selected video gets displayed in fullscreen mode with full volume audio. While the video is playing in fullscreen mode, keyworks that are spoken in the video start to appear in text over the playing video. If the user selects a keyword, the video will fast-forward to that section in the video where the selected keyword is located. This feature is time-coded keyword video search.

![image](https://bytebucket.org/martha_ladly/cbcholodeckweb/raw/04b934b257f4767ed9a9b36bb155c55c92043818/readmeImages/screenShot.png)

### Installation

The CBC Holodeck web version is fully client side application. All of it's 3rd party dependencies are included in the repo as well. This means that to install the CBC web  application you can either download the [zip file](https://bitbucket.org/martha_ladly/cbcholodeckweb/downloads) which will not include the projets git history that allows you to contribute to the future development of the project or you can clone the repo to you machine, contribute and commit improvements back to the project. To clone the project you will need Terminal (OSX) and will use the command `git clone https://martha_ladly@bitbucket.org/martha_ladly/cbcholodeckweb.git`

Once the project is downloaded or cloned you will need to add the archived CBC videos to the project. See the `Media` section of this README.md file for instructions. The reason the videos are not in the repo is that OCAD U and the VALab do not have permission to make the CBC videos public. 

**DO NOT ADD THE CBC VIDEOS TO THIS REPO**

### Run

The CBC Holodeck web version is completly client side. This means that to run the application the user must have the project downloaded (see above). Once downloaded open the `index.html` file using your favorite web browser. For the best user experience the CBC web application is best used with a touch screen. 

### Interactions + Behaviour

The CBC Holodeck web version works best on touch screen devices such as tablets. On a tablet there are many different interaction gestures that the user can preform to interact with the application.

On the index page (`index.html`) a **single tap** of a video will focus the audio of that video by increasing the volume over the other videos in the grid.

**Double tapping** a video on the index page will navigate the user to the video details page where the video will play in fullscreen mode. Take note that each video has it's own html detail page. 

Now that we are on the video detail page, (fullscreen mode) we have some new interaction gestures that we can now use.

**Swiping with one finger** or **pinching the screen with two fingers** will close fullscreen mode and navigate the user back to the landing page.

Once the keywords start to display overtop of the video a **single tap of a keyword** will fast-forward the video to where that selected keywork is spoken and keep the existing keywords overlayed on top of the video. If the user **double taps a keyword**, the video will fast-forward to where the keyword is spoken but will also clear the keyword overlays displaying over the video.

### Folder Structure

The main folders in the project are:

* `/js/`
* `/video/`

We discuss the `/video/` directory in the `Media` section so lets look at the `/js/` folder. The `/js/` folder is where all of the JavaScript files for the application are located. Both library dependencies and custom written JavaScript files for the application are located in this folder.

Note that all of the `.css` files are located in the top level directory of the project. The CSS file that will be of most interest will be the `index.css` file as it included in the `index.html` file.

`index.html` includes two CSS files `index.css` and `video-js.css` where the latter is a styling dependency for `video.js`.


### Media

**DO NOT ADD THE CBC VIDEOS TO THIS REPO**

Since the CBC media is not allowed to be shared online we have not uploaded the videos with the repo. You can update the file source to use different videos OR you can find the original CBC videos on the Lenovo laptop in the VALab tech cabinet and use them on your local machine. In your project create a folder named `/video/` in the top level directory of the project and put the CBC movies in this folder.

### Dependencies

The CBC Holodeck web version uses custom written JavaScript and a few 3rd party library dependencies which are included in the repo. These files are located in the `/js/` folder.

The custom JavaScript files are:

* jquery.arrayshuffle.js
* script.js
* textNavigation.js

And the JavaScript 3rd party dependencies are:

* [jquery v.1.7.2](https://jquery.com/download/)
* [video.js v.3.2.0](https://github.com/videojs/video.js)
* [html5.js (shiv)](https://github.com/aFarkas/html5shiv)
* [html5lightbox.js](https://html5box.com/html5lightbox/) 
* [jquery.hammer.min.js](http://hammerjs.github.io/)
* [jquery.lightbox_me.js](http://buckwilson.me/lightboxme/)
* [jquery.timers.js (Mystery library ????)](http://mysterylibrary.io)
* [marquee.js](https://github.com/aamirafridi/jQuery.Marquee)


