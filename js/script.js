var classChangeInterval, transitionDuration, vid, classOptions, trueRand, classIndex;

classChangeInterval = 8000;
transitionDuration = 350;
vid = document.getElementById("vid_container");
$vid = $(vid);
classOptions = ['p1','p2','p3','p4', 'p5'];
classIndex = 1;

/**
  * set this value to true or false, depending on if you want
  * the display of the video to be TRULY random, or to merely
  * randomize the order of display positions, and then iterate
  * sequentially through that order.
  */
trueRand = false;

function randclass(){
  numOptions = classOptions.length;
  //If it's not truly random, we want to ITERATE through a
  //randomly sorted instance of the array.
  if(!trueRand){
    randClass = classOptions[classIndex-1];
    if(classIndex === numOptions){
      classIndex = 1;
      //randomize order again
      classOptions = $.shuffle(classOptions);
    }
    else{
      classIndex++;
    }
  }
  else{
    /**
      * Otherwise, we truly randomly cycle through the array,
      * with the real risk of repitition.
      */
    rand = Math.floor((Math.random()*numOptions));
    randClass = classOptions[rand];
  }
  return randClass;
}

$(document).ready(function(){
  if(!trueRand){
    /**
      * Initially randomly shuffle the order of the array to
      * iterate through, since it's not true random
      */
    classOptions = $.shuffle(classOptions);
  }
  //Set class initially
  newClass = randclass();
  vid.className = newClass;

  //Set up timer loop to change class according to interval
  $(document).everyTime(classChangeInterval,function(){
    //add transition class so it fades quickly
    $vid.addClass("transition");
    //set timer to transition duration, then update class
    $(document).oneTime(transitionDuration,function(){
      //add new, "random" class to video.
      newClass = randclass();
      vid.className = newClass;
    });
  },0);
});







