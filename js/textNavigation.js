function clickification(e){
  var player = $('video').attr('playerid');
  player = window[player];
  var time = $(e).attr("timing");
  console.log(time,player.duration());
  
  player.currentTime(time);
  
}

$(document).ready(function(){
  window.videoPlayer = _V_("player");
  window.video = $(".container");
  window.body = $("body");
  window.holder = $(".holder");
  window.video.hammer();
  window.body.hammer();
  window.holder.hammer();

  window.body.on("swipe",function(){
    var destination = "index.html";
    window.location = destination;
  });

  videoPlayer.ready(function(){
    window.setTimeout(animateText,6000);
    // $(".container").mouseleave(function(){
    //   var links = $(this).find('.overlayText a');
    //   links.css({'opacity':'0'});
    // });

    window.body.on("tap", function(e){

      if(e.target !== this){
        return;
      }
      var links = $(this).find('.overlayText a');
      links.css({'opacity':'0'});
      links.stop(true).clearQueue();
    });

    window.holder.on("tap", function(e){

      if(e.target !== this){
        return;
      }
      var links = $(this).find('.overlayText a');
      links.css({'opacity':'0'});
      links.stop(true).clearQueue();
    });

    window.video.on("doubletap",function(){
	  animateText();
    });
  });
});


function getRandomInt (min, max) {
  return Math.floor(Math.random()*(max - min + 1)) + min;
}
function animateText(){
  links = $('.overlayText a');
  links.stop();
  links.each(function(i){
	var top, left, maxTop, minTop, maxLeft, minLeft;
	$(this).css({'opacity':'0'});
	maxLeft = 1024-$(this).width();
	minLeft = 0;
	maxTop = 768-$(this).height();
	minTop = 0;
	top = getRandomInt(maxTop,minTop);
	left = getRandomInt(maxLeft,minLeft);
	
	$(this).stop().delay((i++)*1000).animate({'top':top,'left':left},function(){
	  $(this).animate({'opacity':'1'},200);

	  $(this).unbind("click").click(function(e){
		clickification(this);
	  });
	});
  });
}